const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
    res.send('Hello World')
    setInterval(() => {
        console.log("abcdefghiklm")
    }, 10000)
})

//Blogs
const  BlogsController = require('./controllers/BlogsController')
router.get('/allBlogs',BlogsController.getPagination)
router.post('/create-Blog', BlogsController.createBlog)
router.put('/update-blog/:blogId', BlogsController.updateBlog)
router.delete('/delete-blog/:blogId', BlogsController.deleteBlog)

//Categories
const CategoriesController = require('./controllers/CategoriesController')
router.get('/search-category/:categoryId', CategoriesController.searchCategory)
module.exports = router