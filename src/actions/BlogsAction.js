const BlogModel = require('../models/Blog')
const CategoryModel = require('../models/Category')
const Joi = require('joi')

/**
 * Pagination Blog
 * @param {Object} param0
 * @param {number} param0.limit - default limit = 20
 * @param {number} param0.page - default page = 1
 * @returns {Promise<Query<LeanDocument<unknown extends {_id?: infer U} ? IfAny<U, {_id: Types.ObjectId}, Required<{_id: U}>> : {_id: Types.ObjectId}>[], Document<unknown, any, unknown> & unknown extends {_id?: infer U} ? IfAny<U, {_id: Types.ObjectId}, Required<{_id: U}>> : {_id: Types.ObjectId}, unknown, unknown>>}
 */
exports.getPagination = async({limit=20, page=1}) => {
    return BlogModel.find({})
        .populate({
            model: CategoryModel,
            path: 'category_id'
        })
        .limit(limit)
        .skip(limit*(page-1))
        .lean()
}

/**
 *
 * @param {Object} args
 * @returns {Promise<Joi.AsyncValidationOptions extends ({artifacts: true} | {warnings: true}) ? ({value: any} & Joi.AsyncValidationOptions extends {artifacts: true} ? {artifacts: Map<any, string[][]>} : {} & Joi.AsyncValidationOptions extends {warnings: true} ? {warning: Joi.ValidationError[]} : {}) : any>}
 */
const validate = async(args)=>{
    const schema = Joi.object({
        title:  Joi.string().required(),
        author: Joi.string(),
        description:  Joi.string(),
        content: Joi.string(),
    })
    return schema.validateAsync(args)
}
/**
 *
 * @param {Object} args
 * @returns {Promise<HydratedDocument<InferSchemaType<*>, ObtainSchemaGeneric<*, "TInstanceMethods">, {}>[]>}
 */
exports.createBlog = async(args) => {
    const validateArgs = await validate(args)
    return BlogModel.create(validateArgs)
}

/**
 * Update Blog
 * @param {Object} param0
 * @param {string} param0.blogId
 * @param {Object} param0.args
 * @returns {Promise<Query<Document<unknown, any, unknown> & unknown extends {_id?: infer U} ? IfAny<U, {_id: Types.ObjectId}, Required<{_id: U}>> : {_id: Types.ObjectId}, Document<unknown, any, unknown> & unknown extends {_id?: infer U} ? IfAny<U, {_id: Types.ObjectId}, Required<{_id: U}>> : {_id: Types.ObjectId}, unknown, unknown>>}
 */
exports.updateBlog = async({args, blogId}) => {
    const validateArgs = await validate(args)
    await BlogModel.findOneAndUpdate({'_id': blogId}, {$set: validateArgs})
    return BlogModel.findOne({'_id': blogId}).lean()
}

/**
 * Delete Blog
 * @param {ObjectId} blogId
 * @returns {Promise<Query<DeleteResult, Document<unknown, any, unknown> & unknown extends {_id?: infer U} ? IfAny<U, {_id: Types.ObjectId}, Required<{_id: U}>> : {_id: Types.ObjectId}, unknown, unknown>>}
 */
exports.deleteBlog = async(blogId) => {
    return BlogModel.deleteOne({'_id': blogId})
}
/**
 *
 * @param {ObjectId} categoryId
 * @returns {Promise<Query<LeanDocument<InferSchemaType<module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, {}, {}, DefaultTypeKey, {creatAt: {default: () => number, type: DateConstructor}, category_id: {ref: string, type: ObjectId}, author: StringConstructor, description: StringConstructor, title: StringConstructor, content: StringConstructor}>> & {_id: Types.ObjectId}>[], Document<unknown, any, InferSchemaType<module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, {}, {}, DefaultTypeKey, {creatAt: {default: () => number, type: DateConstructor}, category_id: {ref: string, type: ObjectId}, author: StringConstructor, description: StringConstructor, title: StringConstructor, content: StringConstructor}>>> & InferSchemaType<module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, {}, {}, DefaultTypeKey, {creatAt: {default: () => number, type: DateConstructor}, category_id: {ref: string, type: ObjectId}, author: StringConstructor, description: StringConstructor, title: StringConstructor, content: StringConstructor}>> & {_id: Types.ObjectId} & ObtainSchemaGeneric<module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, {}, {}, DefaultTypeKey, {creatAt: {default: () => number, type: DateConstructor}, category_id: {ref: string, type: ObjectId}, author: StringConstructor, description: StringConstructor, title: StringConstructor, content: StringConstructor}>, "TInstanceMethods">, ObtainSchemaGeneric<module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, {}, {}, DefaultTypeKey, {creatAt: {default: () => number, type: DateConstructor}, category_id: {ref: string, type: ObjectId}, author: StringConstructor, description: StringConstructor, title: StringConstructor, content: StringConstructor}>, "TQueryHelpers">, InferSchemaType<module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, {}, {}, DefaultTypeKey, {creatAt: {default: () => number, type: DateConstructor}, category_id: {ref: string, type: ObjectId}, author: StringConstructor, description: StringConstructor, title: StringConstructor, content: StringConstructor}>>> & ObtainSchemaGeneric<module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, {}, {}, DefaultTypeKey, {creatAt: {default: () => number, type: DateConstructor}, category_id: {ref: string, type: ObjectId}, author: StringConstructor, description: StringConstructor, title: StringConstructor, content: StringConstructor}>, "TQueryHelpers">>}
 */
exports.searchCategory = async(categoryId) => {
    const data = BlogModel.find({category_id: categoryId}).lean()
    return data
}
