const mongoose = require('mongoose')

exports.initDBMongo =async () => {
    try {
        await mongoose.connect('mongodb://localhost:27017/blogs')
        console.log('connection')
        return mongoose
    } catch (error) {
        console.log('connection error')
    }
}
