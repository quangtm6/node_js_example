const mongoose = require('mongoose')
const {Schema} = mongoose

const blogSchema = new Schema({
    title: String,
    description: String,
    author: String,
    content: String,
    creatAt: {
        type: Date,
        default: Date.now
    },
    category_id: {
        type: Schema.Types.ObjectId,
        ref: 'Categories'
    }
})

module.exports = mongoose.model('Blog', blogSchema)