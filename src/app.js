const express = require('express')
const app = express()
const router = require('./app.router')
const bodyParser = require('body-parser')
const port = 3004
const { initDBMongo } = require('./connections/mongodb')

setTimeout(async () => {
    await initDBMongo()
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    app.use(router)
    app.listen(port, () => {
        console.log(`Run listening on port ${port}`)
    })
}, 0)

// setInterval(async () => {
//     app.get('/repeat', (req,res) => {
//         res.send('Snake')
//     })
//     console.log('abcdefghik')
// }, 10000)
// app.listen(port, () => {
//     console.log(`Run listening on port ${port}`)
// })
