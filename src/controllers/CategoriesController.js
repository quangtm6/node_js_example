const BlogsAction = require('../actions/BlogsAction')

exports.searchCategory = async (req, res) => {
    const {categoryId} = req.params
    try {
        const data = await BlogsAction.searchCategory(categoryId)
        res.send({status: 'success', data})
    } catch (error) {
        res.status(500).json({status: 'error'})
    }
}