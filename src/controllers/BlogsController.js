const BlogsAction = require('../actions/BlogsAction')

exports.getPagination = async(req, res) => {
    const {limit, page} = req.query
    try {
        // goi function get blogs
       const data= await BlogsAction.getPagination({limit, page})
        res.send({
            status:'success',
            data
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({error: 'error'})
    }
}
exports.createBlog = async (req, res) => {
    console.log(req.body)
    const args = Object.assign({}, req.body)
    try {
        const data = await BlogsAction.createBlog(args)
        res.send({status: 'success', data})
    } catch (error) {
        res.status(500).json({error: 'error'})
    }
}

exports.updateBlog = async (req, res) => {
    const {blogId} = req.params
    const args = Object.assign({}, req.body)
    try {
        const data = await BlogsAction.updateBlog({args, blogId})
        res.send({status: 'success', data})
    } catch (error) {
        res.status(500).json({error: 'error'})
    }
}

exports.deleteBlog = async (req, res) => {
    const {blogId} = req.params
    try {
        await BlogsAction.deleteBlog(blogId)
        res.send({status: 'success'})
    } catch (error) {
        res.status(500).json({error: 'error'})
    }
}


